package ${package};

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Component
public class EndpointsListener implements ApplicationListener<ApplicationEvent>
{
	private final Logger LOGGER = LoggerFactory.getLogger(EndpointsListener.class);

	@Override
	public void onApplicationEvent(ApplicationEvent event)
	{
		if (event instanceof ContextRefreshedEvent)
		{
			ApplicationContext applicationContext = ((ContextRefreshedEvent) event).getApplicationContext();
			Map<RequestMappingInfo, HandlerMethod> endpoints = applicationContext.getBean(RequestMappingHandlerMapping.class).getHandlerMethods();

			LOGGER.info("Registered REST-Endpoints: ");
			for (RequestMappingInfo mappingInfo : endpoints.keySet())
				LOGGER.info(mappingInfo + " -> " + endpoints.get(mappingInfo));
		}
	}
}
