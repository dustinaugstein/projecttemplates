package ${package}.rest.about;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AboutAPI 
{
	@Value("${r"${about.version}"}")
	private String version;
	
	@RequestMapping("/about")
	public String about() 
	{
		 return version;
	}
}
