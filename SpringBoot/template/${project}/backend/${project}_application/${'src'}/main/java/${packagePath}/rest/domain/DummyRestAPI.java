package ${package}.rest.domain;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ${package}.reflective_translator.ReflectiveTranslator;
import ${package}.rest.dto.DummyDTO;

import ${package}.reflective_translator.ReflectiveTranslator;
import ${package}.rest.dto.DummyDTO;
import ${package}.service.ServiceLayer;
import ${package}.service.model.DummyModel;

@RestController
public class DummyRestAPI
{
	@Autowired
	private ServiceLayer service;

	@Value("server.port")
	private String serverPort;

	@RequestMapping(path = "/Dummy", method = RequestMethod.GET)
	public ResponseEntity<Collection<DummyDTO>> getDummys()
	{
		Collection<DummyDTO> result = ReflectiveTranslator.translate(DummyDTO.class, service.getDummies());
		return ResponseEntity.ok(result);
	}

	@RequestMapping(path = "/Dummy/{id}", method = RequestMethod.GET)
	public ResponseEntity<DummyDTO> getDummy(@PathVariable String id) {
		DummyDTO result = ReflectiveTranslator.translate(DummyDTO.class, service.getDummy(id));
		return ResponseEntity.ok(result);
	}

	@RequestMapping(path = "/Dummy/{id}", method = RequestMethod.PUT)
	public ResponseEntity<DummyDTO> updateDummy(@PathVariable Long id, @RequestBody DummyDTO DummyDTO)
	{
		DummyModel dummyModel = this.service.updateDummy(ReflectiveTranslator.translate(DummyModel.class, DummyDTO));
		DummyDTO result = ReflectiveTranslator.translate(DummyDTO.class, dummyModel);
		return ResponseEntity.ok(result);
	}

	@RequestMapping(path = "/Dummy", method = RequestMethod.POST)
	public ResponseEntity<String> createDummy(@RequestBody DummyDTO DummyDTO)
	{
		if (DummyDTO.getId() != null)
			return new ResponseEntity<String>("ID of Model should be null! It's defined by serverside.", HttpStatus.BAD_REQUEST);
		DummyModel result = this.service.createDummy(ReflectiveTranslator.translate(DummyModel.class, DummyDTO));
		return ResponseEntity.created(generteURI(result.getId())).build();
	}

	@RequestMapping(path = "/Dummy/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteDummy(@PathVariable String id)
	{
		this.service.deleteDummy(id);
		return ResponseEntity.ok().build();
	}

	// TODO: this has to be refactored!
	private URI generteURI(String id)
	{
		try
		{
			return new URI("http://localhost:" + serverPort + "Dummy/" + id);
		} catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
