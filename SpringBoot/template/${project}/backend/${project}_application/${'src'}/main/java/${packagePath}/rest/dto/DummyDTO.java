package ${package}.rest.dto;

import lombok.Data;

@Data
public class DummyDTO
{
	private Long id;

	private String name;

	public DummyDTO()
	{
	}
}
