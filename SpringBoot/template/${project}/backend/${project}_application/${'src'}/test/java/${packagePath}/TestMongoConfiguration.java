package ${package};

import org.springframework.context.annotation.Configuration;

import com.mongodb.client.MongoClients;

import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableMongoAuditing
public class TestMongoConfiguration extends AbstractMongoClientConfiguration
{
	private MongoServer server;
    
    @Value("${r"${mongodb.name}"}")
	protected String dbName;

	@Value("${r"${mongodb.url}"}")
	protected String dbUrl;

	@Value("${r"${mongodb.port}"}")
	protected int dbPort;

    @Bean
    @Override
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory(), mappingMongoConverter());
    }

	@Override
	protected String getDatabaseName()
	{
		return dbName;
	}

	@Bean
    @Override
	public com.mongodb.client.MongoClient mongoClient() {
		this.server = new MongoServer(new MemoryBackend());
		this.server.bind(this.dbUrl, this.dbPort);
		return MongoClients.create("mongodb://" + this.dbUrl + ":" + this.dbPort);
	}
}