package ${package}.access.mongodb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.client.MongoClients;

import org.springframework.context.annotation.Bean;

@Configuration
@EnableMongoAuditing
public class MongoConfiguration extends AbstractMongoClientConfiguration
{
	@Value("${r"${mongodb.url}"}")
	protected String dbUrl;
	
	@Value("${r"${mongodb.name}"}")
	protected String dbName;

	@Bean
	@Override
	public MongoTemplate mongoTemplate() throws Exception
	{
		return new MongoTemplate(mongoDbFactory(), mappingMongoConverter());
	}

	@Override
	protected String getDatabaseName()
	{
		return dbName;
	}

	@Bean
	@Override
	public com.mongodb.client.MongoClient mongoClient()
	{
		return MongoClients.create(this.dbUrl);
	}
}