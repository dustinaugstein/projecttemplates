package ${package}.access.mongodb.dataobject;

import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AbstractDO
{
	@Id
	private String id;

	public AbstractDO()
	{
	}

	public AbstractDO(String id)
	{
		this.id = id;
	}
}
