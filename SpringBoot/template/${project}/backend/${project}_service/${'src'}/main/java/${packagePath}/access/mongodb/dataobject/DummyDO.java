package ${package}.access.mongodb.dataobject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DummyModelDAO")
public class DummyDO extends AbstractDO 
{
	@Getter
	@Setter
	@Column(name = "name")
	private String name;
	
	public DummyDO()
	{
		super();
	}
	
	public DummyDO(String id)
	{
		super(id);
	}
}
