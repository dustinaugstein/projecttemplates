package ${package}.access.mongodb.implementation;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import ${package}.access.mongodb.dataobject.AbstractDO;
import ${package}.access.mongodb.repository.IBaseRepository;


public abstract class AbstractAccess<T, O extends AbstractDO, R extends IBaseRepository<O>>
{
	@Autowired
	protected R repository;
	
	protected abstract T create(T DummyModel);
	
	protected abstract T update(T DummyModel);

	public abstract T findById(String id);
	
	public abstract Collection<T> findAll();
}