package ${package}.access.mongodb.implementation;

import org.springframework.stereotype.Component;

import ${package}.access.mongodb.dataobject.DummyDO;
import ${package}.access.mongodb.repository.DummyRepository;
import ${package}.service.model.DummyModel;

@Component
public class DummyAccess extends AbstractGenericAccess<DummyModel, DummyDO, DummyRepository>
{
	public DummyAccess()
	{
		super(DummyModel.class, DummyDO.class);
	}
}
