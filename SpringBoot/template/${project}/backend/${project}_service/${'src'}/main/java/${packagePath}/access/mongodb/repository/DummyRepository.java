package ${package}.access.mongodb.repository;

import org.springframework.stereotype.Repository;

import ${package}.access.mongodb.dataobject.DummyDO;

@Repository
public interface DummyRepository extends IBaseRepository<DummyDO>
{
}
