package ${package}.access.mongodb.translator;

import java.util.ArrayList;
import java.util.List;

import ${package}.access.mongodb.dataobject.DummyDO;
import ${package}.service.model.DummyModel;

public class DataObjectToModelTranslator
{
	public static List<DummyModel> translate(List<DummyDO> DummyDAOs)
	{
		List<DummyModel> result = new ArrayList<DummyModel>();
		for (DummyDO DummyDAO : DummyDAOs)
			result.add(translate(DummyDAO));
		return result;
	}
	
	public static DummyModel translate(DummyDO DummyDAO)
	{
		DummyModel result = new DummyModel();
		result.setId(DummyDAO.getId());
		result.setName(DummyDAO.getName());
		return result;
	}
}
