package ${package}.access.mongodb.translator;

import java.util.ArrayList;
import java.util.List;

import ${package}.access.mongodb.dataobject.DummyDO;
import ${package}.service.model.DummyModel;

public class ModelToDataObjectTranslator
{
	public static List<DummyDO> translate(List<DummyModel> DummyModels)
	{
		List<DummyDO> result = new ArrayList<DummyDO>();
		for (DummyModel DummyModel : DummyModels)
			result.add(translate(DummyModel));
		return result;
	}

	public static DummyDO translate(DummyModel DummyModel)
	{
		DummyDO result = new DummyDO();
		result.setId(DummyModel.getId());
		result.setName(DummyModel.getName());
		return result;
	}
}
