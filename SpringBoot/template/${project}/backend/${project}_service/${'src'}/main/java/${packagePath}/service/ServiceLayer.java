package ${package}.service;

import java.util.Collection;

import ${package}.service.model.DummyModel;

public interface ServiceLayer
{
	public DummyModel processDummy(DummyModel dummyModel);

	public DummyModel updateDummy(DummyModel dummyModel);

	public DummyModel createDummy(DummyModel dummyModel);

	public Collection<DummyModel> getDummies();

	public DummyModel getDummy(String id);

	public void deleteDummy(String id);
}
