package ${package}.service.implementation;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

<#if mongodb.enabled>
import ${package}.access.mongodb.implementation.DummyAccess;
</#if>
import ${package}.service.ServiceLayer;
import ${package}.service.model.DummyModel;

@Service
public class ServiceLayerImp implements ServiceLayer
{
	<#if mongodb.enabled>
	@Autowired
	private DummyAccess DummyModelAccess;
	</#if>
	
	@Override
	public DummyModel updateDummy(DummyModel DummyModel)
	{
		<#if mongodb.enabled>
		return this.DummyModelAccess.update(DummyModel);
		<#else>
		return null;
		</#if>
	}

	@Override
	public DummyModel createDummy(DummyModel DummyModel)
	{
		<#if mongodb.enabled>
		return this.DummyModelAccess.create(DummyModel);
		<#else>
		return null;
		</#if>
	}

	@Override
	public Collection<DummyModel> getDummies()
	{
		<#if mongodb.enabled>
		return this.DummyModelAccess.findAll();
		<#else>
		return null;
		</#if>
	}

	@Override
	public DummyModel getDummy(String id)
	{
		<#if mongodb.enabled>
		return this.DummyModelAccess.findById(id);
		<#else>
		return null;
		</#if>
	}

	@Override
	public void deleteDummy(String id)
	{
		<#if mongodb.enabled>
		this.DummyModelAccess.deleteById(id);
		</#if>
	}

	@Override
	public DummyModel processDummy(DummyModel DummyModel)
	{
		<#if mongodb.enabled>
		DummyModel result = null;
		if (DummyModel.getId() != null && this.DummyModelAccess.existsById(DummyModel.getId()))
			result = this.DummyModelAccess.update(DummyModel);
		else
			result = this.DummyModelAccess.create(DummyModel);
		return result;
		<#else>
		return null;
		</#if>
	}
}
