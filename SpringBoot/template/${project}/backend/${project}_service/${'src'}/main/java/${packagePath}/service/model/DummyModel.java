package ${package}.service.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class DummyModel extends AbstractModel
{
	private String name;

	public DummyModel()
	{
		super();
	}

	public DummyModel(String id)
	{
		super(id);
	}
}
