package ${package};

import org.springframework.context.annotation.Configuration;

import com.mongodb.client.MongoClients;

import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.util.SocketUtils;


@Configuration
@EnableMongoAuditing
public class TestMongoConfiguration extends AbstractMongoClientConfiguration
{
	private MongoServer server;
    
    @Bean
    @Override
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory(), mappingMongoConverter());
    }

	@Override
	public String getDatabaseName() {
		return "DevenorTest";
	}

	@Bean
    @Override
	public com.mongodb.client.MongoClient mongoClient() {
		int port = SocketUtils.findAvailableTcpPort();
		this.server = new MongoServer(new MemoryBackend());
		this.server.bind("localhost", port);
		return MongoClients.create("mongodb://localhost:" + port);
	}
}