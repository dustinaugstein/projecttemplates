package ${package};

import java.io.IOException;
import java.util.List;

import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;

public class TestYMLConfigLoader implements ApplicationContextInitializer<ConfigurableApplicationContext>
{
	@Override
	public void initialize(ConfigurableApplicationContext applicationContext)
	{
		try
		{
			Resource resource = applicationContext.getResource("classpath:application.yml");
			YamlPropertySourceLoader sourceLoader = new YamlPropertySourceLoader();
			List<PropertySource<?>> yamlTestProperties = sourceLoader.load("yamlProperties", resource);
			for (PropertySource<?> yamlTestProperty : yamlTestProperties)
				applicationContext.getEnvironment().getPropertySources().addFirst(yamlTestProperty);
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
}