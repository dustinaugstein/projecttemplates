package ${package}.service;

import ${package}.service.model.DummyModel;

public abstract class AbstractServiceLayerTest
{
	protected DummyModel generateDummyModel(String id)
	{
		DummyModel result = new DummyModel();
		result.setId(id);
		result.setName("Name" + id);
		return result;
	}
}
