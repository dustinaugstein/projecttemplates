package ${package}.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ${package}.TestApplicationConfiguration;
<#if mongodb.enabled>
import ${package}.TestMongoConfiguration;
</#if>
import ${package}.TestYMLConfigLoader;
import ${package}.access.exception.ModelNotFoundException;
import ${package}.service.model.DummyModel;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestApplicationConfiguration.class,<#if mongodb.enabled> TestMongoConfiguration.class </#if>}, initializers = TestYMLConfigLoader.class)
public class ServiceLayerTest extends AbstractServiceLayerTest
{
	<#if mongodb.enabled>
	@Autowired
	private ServiceLayer service;

	@Test
	public void createDummy()
	{
		DummyModel createdDummyModel = generateDummyModel(null);
		createdDummyModel = this.service.createDummy(createdDummyModel);
		assertNotNull(createdDummyModel);
		assertNotNull(createdDummyModel.getId());
		this.service.deleteDummy(createdDummyModel.getId());
	}

	@Test
	public void updateDummy()
	{
		DummyModel createdDummyModel = generateDummyModel(null);
		createdDummyModel = this.service.createDummy(createdDummyModel);
		DummyModel loadedDummyModel = this.service.getDummy(createdDummyModel.getId());
		assertNotNull(loadedDummyModel);
		assertEquals(createdDummyModel.getName(), loadedDummyModel.getName());

		loadedDummyModel.setName("NewName");
		this.service.updateDummy(loadedDummyModel);
		DummyModel loadedDummyModel2 = this.service.getDummy(loadedDummyModel.getId());
		assertNotNull(loadedDummyModel2);
		assertEquals(loadedDummyModel.getName(), loadedDummyModel2.getName());

		this.service.deleteDummy(loadedDummyModel.getId());
	}

	@Test
	public void getDummy()
	{
		DummyModel createdDummyModel = generateDummyModel(null);
		createdDummyModel = this.service.createDummy(createdDummyModel);
		DummyModel loadedDummyModel = this.service.getDummy(createdDummyModel.getId());
		assertNotNull(loadedDummyModel);
		assertEquals(createdDummyModel.getName(), loadedDummyModel.getName());
		this.service.deleteDummy(loadedDummyModel.getId());
	}

	@Test
	public void getDummys()
	{
		DummyModel createdDummyModel1 = generateDummyModel(null);
		DummyModel createdDummyModel2 = generateDummyModel(null);
		this.service.createDummy(createdDummyModel1);
		this.service.createDummy(createdDummyModel2);

		Collection<DummyModel> loadedDummyModels = this.service.getDummies();
		assertNotNull(loadedDummyModels);
		assertEquals(2, loadedDummyModels.size());

		for (DummyModel loadedDummyModel : loadedDummyModels)
			this.service.deleteDummy(loadedDummyModel.getId());
	}

	@Test
	public void deleteDummy()
	{
		DummyModel createdDummyModel = generateDummyModel(null);
		createdDummyModel = this.service.createDummy(createdDummyModel);
		DummyModel loadedDummyModel = this.service.getDummy(createdDummyModel.getId());
		assertNotNull(loadedDummyModel);
		assertEquals(createdDummyModel.getName(), loadedDummyModel.getName());
		this.service.deleteDummy(loadedDummyModel.getId());
		try
		{
			loadedDummyModel = this.service.getDummy(createdDummyModel.getId());
			assertTrue("ModelNotFoundException should be thrown!", false);
		} catch (ModelNotFoundException ex)
		{
		}
	}
	<#else>
	@Test
	public void dummyTest()
	{
		assertTrue(true);
	}
	</#if>
}
