mvn clean install
docker build . -t ${k8s.imageRegistry}/${project}:latest
docker push ${k8s.imageRegistry}/${project}:latest